package moneygo1.moneygo1.config

import moneygo1.moneygo1.entity.Activity
import moneygo1.moneygo1.entity.User
import moneygo1.moneygo1.entity.WalletStatus
import moneygo1.moneygo1.repository.ActivityRepository
import moneygo1.moneygo1.repository.UserRepository
import org.apache.poi.ss.usermodel.CellType
import org.apache.poi.ss.usermodel.Row
import org.apache.poi.ss.util.CellReference
import org.apache.poi.ss.util.CellUtil
import org.apache.poi.xssf.usermodel.XSSFWorkbook
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.core.io.ClassPathResource
import org.springframework.stereotype.Component
import java.io.IOException
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import javax.transaction.Transactional

@Component
class DataLoader {
    @Value("\${xlsxPath}")
    val xlsxPath: String? = null
    @Autowired
    lateinit var userRepository: UserRepository
    @Autowired
    lateinit var activityRepository: ActivityRepository


    fun loadData() {
        createActivityData()
        createUserData()
    }

    fun getCellData(row: Row, col: String): String {
        val colIdx = CellReference.convertColStringToIndex(col)
        return getCellData(row, colIdx)
    }

    fun getCellData(row: Row, colIdx: Int): String {
        val cell = CellUtil.getCell(row, colIdx)
        if (cell.cellType == CellType.STRING) {
            if (cell.stringCellValue == "NULL")
                return ""
            return cell.stringCellValue
        } else if (cell.cellType == CellType.NUMERIC) {
            cell.cellType = CellType.STRING
            return cell.stringCellValue
        }
        return ""
    }

    @Transactional
    fun createUserData() {
        try {
            val file = xlsxPath?.let { ClassPathResource(it).inputStream }
            val workbook = XSSFWorkbook(file)
            val sheet = workbook.getSheet("User")
            val rowIterator = sheet.iterator()
            rowIterator.next()
            while (rowIterator.hasNext()) {
                val row = rowIterator.next()
                if (getCellData(row, "A") != "") {
                    userRepository.save(User(getCellData(row, "A"), getCellData(row, "B")))
                }
            }
        } catch (e: IOException) {

        }
    }

    @Transactional
    fun createActivityData() {
        try {
            val file = xlsxPath?.let { ClassPathResource(it).inputStream }
            val workbook = XSSFWorkbook(file)
            val sheet = workbook.getSheet("Activities")
            val rowIterator = sheet.iterator()
            rowIterator.next()
            while (rowIterator.hasNext()) {
                val row = rowIterator.next()
                if (getCellData(row, "A") != "") {
                    var act = Activity(name = getCellData(row, "A"),
                            walletStatus = WalletStatus.valueOf(getCellData(row, "B")),
                            category = getCellData(row, "C"),
                            description = getCellData(row, "D"),
                            money = getCellData(row, "E").toDouble(),
                            balance = getCellData(row, "F").toDouble(),
                            localDate = LocalDate.parse(getCellData(row, "G"), DateTimeFormatter.ofPattern("yyyy/MM/dd"))
                            )
                    val user = userRepository.findByName(getCellData(row, "H"))
                    user?.let {
                        act.user = user
                        user.activity.add(act)
                    }
                    activityRepository.save(act)
                }
            }
        } catch (e: IOException) {

        }
    }
}