package moneygo1.moneygo1.config

import moneygo1.moneygo1.entity.Activity
import moneygo1.moneygo1.entity.User
import moneygo1.moneygo1.entity.WalletStatus
import moneygo1.moneygo1.repository.ActivityRepository
import moneygo1.moneygo1.repository.UserRepository
import moneygo1.moneygo1.security.entity.Authority
import moneygo1.moneygo1.security.entity.AuthorityName
import moneygo1.moneygo1.security.entity.JwtUser
import moneygo1.moneygo1.security.repository.AuthorityRepository
import moneygo1.moneygo1.security.repository.JwtUserRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.ApplicationArguments
import org.springframework.boot.ApplicationRunner
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.stereotype.Component
import java.time.LocalDate
import javax.transaction.Transactional

@Component
class ApplicationLoader: ApplicationRunner{

    @Autowired
    lateinit var userRepository : UserRepository
    @Autowired
    lateinit var activityRepository: ActivityRepository
    @Autowired
    lateinit var dataLoader: DataLoader
    @Autowired
    lateinit var authorityRepository: AuthorityRepository
    @Autowired
    lateinit var jwtUserRepository: JwtUserRepository

    @Transactional
    fun loadUsernameAndPassword(){
        val auth1 = Authority(name = AuthorityName.ROLE_ADMIN)
        val auth2 = Authority(name = AuthorityName.ROLE_CUSTOMER)
        val auth3 = Authority(name = AuthorityName.ROLE_GENERAL)
        authorityRepository.save(auth1)
        authorityRepository.save(auth2)
        authorityRepository.save(auth3)
        val encoder = BCryptPasswordEncoder()
        val user1 = User(name = "Kim", email = "Kim@b.com", imageUrl = "17.jpg")
        val userJwt = JwtUser(
                username = "customer",
                password = encoder.encode("password"),
                email = user1.email,
                enabled = true
        )
        userRepository.save(user1)
        jwtUserRepository.save(userJwt)
        user1.jwtUser = userJwt
        userJwt.user = user1
        userJwt.authorities.add(auth2)
        userJwt.authorities.add(auth3)
    }


    @Transactional
    override fun run(args: ApplicationArguments?) {
        val user1 = userRepository.save(User("Malee","Malee@gmail.com","https://www.thairath.co.th/media/4DQpjUtzLUwmJZZPEbsuLL4qso7oUe7AG8ovZ1BghB1F.jpg"))
        val user2 = userRepository.save(User("Mario","Mario@gmail.com","https://www.thairath.co.th/media/4DQpjUtzLUwmJZZPFiLr9CiebDwTHv6Y3Hq8HdMN5h06.webp"))
        val user3 = userRepository.save(User("Mai","Mai@gmail.com","https://scontent-lga3-1.cdninstagram.com/vp/b5b7f8d9c98155bd935deef6cd46586f/5D33BCF5/t51.2885-15/e35/51612921_422972755136366_7593603345019209137_n.jpg?_nc_ht=scontent-lga3-1.cdninstagram.com"))

        val activity1 = activityRepository.save(Activity("MaleeWallet",WalletStatus.INCOME,"Salary","เดือน ม.ค.",20000.00, 20000.00, LocalDate.parse("2019-03-03")))
        val activity2 = activityRepository.save(Activity("MelloW",WalletStatus.INCOME,"Salary","เดือน ม.ค.",50000.00, 50000.00, LocalDate.parse("2019-03-05")))
        val activity3 = activityRepository.save(Activity("MelloW",WalletStatus.EXPEND,"Travel","Hong Kong",45000.00, 30000.00, LocalDate.parse("2019-03-06")))
        val activity4 = activityRepository.save(Activity("DJ",WalletStatus.INCOME,"Salary","เดือน ม.ค.",50000.00, 50000.00, LocalDate.parse("2019-03-10")))
        val activity5 = activityRepository.save(Activity("DJ",WalletStatus.EXPEND,"Travel","Taipei",25000.00, 25000.00, LocalDate.parse("2019-03-14")))


        user1.activity.add(activity1)
        activity1.user = user1
        user2.activity.add(activity2)
        activity2.user = user2
        user2.activity.add(activity3)
        activity3.user = user2
        user3.activity.add(activity4)
        activity4.user = user3
        user3.activity.add(activity5)
        activity5.user = user3

        dataLoader.loadData()
        loadUsernameAndPassword()
    }
}