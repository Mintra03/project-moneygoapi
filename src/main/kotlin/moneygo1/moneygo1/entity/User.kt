package moneygo1.moneygo1.entity

import moneygo1.moneygo1.security.entity.JwtUser
import javax.persistence.*

@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)

data
//abstract
class User(
        open var name: String? = null,
        open var email: String? = null,
        open var imageUrl: String? = null,
        open var isDeleted: Boolean = false
)  {
    @Id
    @GeneratedValue
    var id:Long? = null

    @OneToMany(mappedBy = "user")
    var activity = mutableListOf<Activity>()

    @OneToOne
    var jwtUser: JwtUser? = null

}