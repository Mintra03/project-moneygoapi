package moneygo1.moneygo1.entity.Dto


data class UserDto (
        var name: String? = null,
        var email: String? = null,
        var imageUrl: String? = null,
        var id:Long? = null,
        var isDeleted: Boolean = false
)

data class UserInfoDto(
        var name: String? = null,
        var email: String? = null,
        var id:Long?=null
)

data class UserImageDto(
        var imageUrl: String? = null,
        var id:Long? = null
)