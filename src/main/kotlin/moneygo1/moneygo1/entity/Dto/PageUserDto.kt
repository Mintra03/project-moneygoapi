package moneygo1.moneygo1.entity.Dto

data class PageUserDto (
        var totalPages: Int? = null,
        var totalElements: Long? = null,
        var users: List<UserDto> = mutableListOf()
)
