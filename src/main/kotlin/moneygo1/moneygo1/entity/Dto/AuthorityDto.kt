package moneygo1.moneygo1.entity.Dto

data class AuthorityDto(
        var name: String? = null
)