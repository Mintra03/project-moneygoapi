package moneygo1.moneygo1.entity.Dto

import moneygo1.moneygo1.entity.WalletStatus
import java.time.LocalDate

data class ActivityDto(
        var name: String? = null,
        var walletStatus: WalletStatus? = WalletStatus.INCOME,
        var category: String? = null,
        var description: String? = null,
        var money: Double? = null,
        var balance: Double? = null,
        var localDate: LocalDate? = null,
        var id:Long? = null
)

data class ActivityInfoDto(
        var name: String? = null,
        var money: Double? = null,
        var id:Long? = null
)