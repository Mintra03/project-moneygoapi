package moneygo1.moneygo1.entity.Dto

import com.sun.imageio.plugins.common.ImageUtil

data class UserRegisterDto (
        var name: String? = null,
        var email: String?=null,
        var password: String?=null,
        var imageUtil: String?=null

)