package moneygo1.moneygo1.entity.Dto

data class UserDto2(
        var name: String?= null,
        var email: String?=null,
        var username: String?=null,
        var authorities: List<AuthorityDto> = mutableListOf()
)