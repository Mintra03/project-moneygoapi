package moneygo1.moneygo1.entity

enum class WalletStatus {
    INCOME, EXPEND, BALANCE
}