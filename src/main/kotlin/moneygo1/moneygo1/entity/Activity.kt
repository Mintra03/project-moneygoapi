package moneygo1.moneygo1.entity

import java.time.LocalDate
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.ManyToOne

@Entity
data class Activity(
        var name: String? = null,
        var walletStatus: WalletStatus? = WalletStatus.INCOME,
        var category: String? = null,
        var description: String? = null,
        var money: Double? = null,
        var balance: Double? = null,
        var localDate: LocalDate? = null,
        var isDeleted: Boolean = false
        ) {

    @Id
    @GeneratedValue
    var id: Long? = null

    @ManyToOne
    lateinit var user: User


}