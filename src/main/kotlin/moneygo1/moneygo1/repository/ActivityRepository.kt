package moneygo1.moneygo1.repository

import moneygo1.moneygo1.entity.Activity
import moneygo1.moneygo1.entity.WalletStatus
import org.springframework.data.repository.CrudRepository

interface ActivityRepository: CrudRepository<Activity,Long> {
    fun findByNameContainingIgnoreCase(name: String): List<Activity>
    fun findByWalletStatus(walletStatus: WalletStatus): List<Activity>
    fun findByCategoryIgnoreCase(category: String): List<Activity>
    fun findByMoney(money: Double): List<Activity>
    fun findByUser_NameContainingIgnoreCase(name: String): List<Activity>
    fun findByIsDeletedIsFalse(): List<Activity>

}