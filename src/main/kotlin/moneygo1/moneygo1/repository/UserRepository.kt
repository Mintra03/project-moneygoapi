package moneygo1.moneygo1.repository

import moneygo1.moneygo1.entity.Dto.UserDto
import moneygo1.moneygo1.entity.User
import org.springframework.data.domain.Pageable
import org.springframework.data.repository.CrudRepository

interface UserRepository: CrudRepository<User, Long> {
    fun findByNameIgnoreCase(name: String): User?
    fun findByNameEndingWith(name: String): List<User>
    fun findByNameContainingIgnoreCaseOrEmailContainingIgnoreCase(name: String, email: String): List<User>
    fun findByNameContainingIgnoreCase(name: String, page:Pageable): List<User>
    fun findByName(cellData: String): User

}