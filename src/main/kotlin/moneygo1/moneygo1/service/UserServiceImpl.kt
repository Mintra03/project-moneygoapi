package moneygo1.moneygo1.service

import moneygo1.moneygo1.dao.UserDao
import moneygo1.moneygo1.entity.Dto.UserDto
import moneygo1.moneygo1.entity.Dto.UserInfoDto
import moneygo1.moneygo1.entity.User
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class UserServiceImpl: UserService {
//    override fun getUserById(id: Long): List<User> {
//        return userDao.getUserById(id)
//    }

    override fun saveImage(id: Long, image: String): User {
        val user = userDao.findById(id)
        user!!.imageUrl = image
        return userDao.save(user)
    }

    override fun saveInfo(id: Long, userInfoDto: UserInfoDto): User? {
        var user = userDao.findById(id)
        user!!.name = userInfoDto.name
        user!!.email = userInfoDto.email
        return userDao.save(user)
    }

    override fun saveRegister(userRegister: User): User {
        return userDao.save(userRegister)
    }

    override fun remove(id: Long): User? {
        val user = userDao.findById(id)
        user?.isDeleted = true
        return user
    }

    override fun save(user: User): User {
        return userDao.save(user)
    }

    override fun getUserWithPage(name: String, page: Int, pageSize: Int): List<User> {
        return userDao.getUserWithPage(name, page, pageSize)
    }

    override fun getByNameEndingWith(name: String): List<User> {
        return userDao.getByNameEndingWith(name)
    }

    override fun getUserByPartialNameIgnoreCaseAndEmailIgnoreCase(name: String, email: String): List<User> {
        return userDao.getUserByPartialNameIgnoreCaseAndEmailIgnoreCase(name, email)
    }

    override fun getUserByPartialNameIgnoreCase(name: String): User? {
        return userDao.getUserByPartialNameIgnoreCase(name)
    }

    @Autowired
    lateinit var userDao: UserDao
    override fun getUsers(): List<User> {
        return userDao.getUsers()
    }
}