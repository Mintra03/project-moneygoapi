package moneygo1.moneygo1.service

import moneygo1.moneygo1.dao.ActivityDao
import moneygo1.moneygo1.dao.UserDao
import moneygo1.moneygo1.entity.Activity
import moneygo1.moneygo1.entity.Dto.ActivityInfoDto
import moneygo1.moneygo1.entity.WalletStatus
import moneygo1.moneygo1.util.MapperUtil
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import javax.transaction.Transactional

@Service
class ActivityServiceImpl:ActivityService {
    override fun saveInfo(activitiesId: Long, activityinfoDto: ActivityInfoDto): Activity? {
        var activity = activityDao.findById(activitiesId)
        activity!!.name = activityinfoDto.name
        activity!!.money = activityinfoDto.money
        return activityDao.save(activity)
    }

    @Transactional
    override fun save(userId: Long, activity: Activity): Activity {
        val user = userDao.findById(userId)
        val activity = activityDao.save(activity)
        user?.activity?.add(activity)
        if (activity.walletStatus == WalletStatus.INCOME) {
            activity.balance = activity.balance!!.plus(activity.money!!)
        } else {
            activity.balance = activity.balance!!.minus(activity.money!!)
        }
        return activity
    }


    override fun addActivities(): List<Activity> {
        return activityDao.getActivities()
    }

    @Transactional
    override fun remove(id: Long): Activity {
        val activity = activityDao.findById(id)
        activity?.isDeleted = true
        return activity
    }

    @Autowired
    lateinit var userDao: UserDao

    @Transactional
    override fun save(activity: Activity): Activity {
//        val user = activity.user?.let { userDao.save(it) }
//        val activity = activityDao.save(activity)
//        activity.user = user
//        val activity = MapperUtil.INSTANCE.mapActivity(activity)
//        return activity
        return activityDao.save(activity)
    }


    override fun getActivitiesByUserNameIgnoreCase(name: String): List<Activity> {
        return activityDao.getActivitiesByUserNameIgnoreCase(name)
    }

    override fun getActivitiesByMoney(money: Double): List<Activity> {
        return activityDao.getActivitiesByMoney(money)
    }

    override fun getActivitiesByCategoryIgnoreCase(category: String): List<Activity> {
        return activityDao.getActivitiesByCategoryIgnoreCase(category)
    }

    override fun getActivitiesByWalletStatus(walletStatus: WalletStatus): List<Activity> {
        return activityDao.getActivitiesByWalletStatus(walletStatus)
    }

    override fun getActivitiesByPartialNameIgnoreCase(name: String): List<Activity> {
        return activityDao.getActivitiesByPartialNameIgnoreCase(name)
    }

    @Autowired
    lateinit var activityDao: ActivityDao
    override fun getActivities(): List<Activity> {
        return activityDao.getActivities()
    }
}