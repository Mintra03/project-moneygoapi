package moneygo1.moneygo1.service

import moneygo1.moneygo1.entity.Activity
import moneygo1.moneygo1.entity.Dto.ActivityDto
import moneygo1.moneygo1.entity.Dto.ActivityInfoDto
import moneygo1.moneygo1.entity.WalletStatus
import javax.persistence.Id

interface ActivityService{
    fun getActivities(): List<Activity>
    fun getActivitiesByPartialNameIgnoreCase(name: String): List<Activity>
    fun getActivitiesByWalletStatus(walletStatus: WalletStatus): List<Activity>
    fun getActivitiesByCategoryIgnoreCase(category: String): List<Activity>
    fun getActivitiesByMoney(money: Double): List<Activity>
    fun getActivitiesByUserNameIgnoreCase(name: String) :List<Activity>
    fun save(activity: Activity): Activity
    fun save(userId: Long, activity: Activity): Activity
    fun remove(id: Long): Activity
    fun addActivities(): List<Activity>
    fun saveInfo(activitiesId: Long, activityinfoDto: ActivityInfoDto): Activity?
}