package moneygo1.moneygo1.service

import moneygo1.moneygo1.entity.Dto.UserDto
import moneygo1.moneygo1.entity.Dto.UserInfoDto
import moneygo1.moneygo1.entity.User

interface UserService {
    fun getUsers(): List<User>
//    fun getUserById(id: Long): List<User>
    fun getUserByPartialNameIgnoreCase(name: String): User?
    fun getUserByPartialNameIgnoreCaseAndEmailIgnoreCase(name: String, email: String): List<User>
    fun getByNameEndingWith(name: String): List<User>
    fun getUserWithPage(name:String, page:Int, pageSize:Int): List<User>
    fun save(user: User): User
    fun remove(id: Long): User?
    fun saveRegister(userRegister: User): User
    fun saveInfo(id: Long, userInfoDto: UserInfoDto): User?
    fun saveImage(id: Long, image: String): User
}