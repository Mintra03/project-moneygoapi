package moneygo1.moneygo1

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class Moneygo1Application

fun main(args: Array<String>) {
    runApplication<Moneygo1Application>(*args)
}
