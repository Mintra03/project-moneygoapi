package moneygo1.moneygo1.util

import com.amazonaws.services.alexaforbusiness.model.UserData
import moneygo1.moneygo1.entity.Activity
import moneygo1.moneygo1.entity.Dto.*
import moneygo1.moneygo1.entity.User
import moneygo1.moneygo1.security.entity.Authority
import org.mapstruct.InheritInverseConfiguration
import org.mapstruct.Mapper
import org.mapstruct.Mapping
import org.mapstruct.Mappings
import org.mapstruct.factory.Mappers

@Mapper(componentModel = "Spring")
interface MapperUtil {
    companion object {
        val INSTANCE = Mappers.getMapper(MapperUtil::class.java)
    }

    fun mapActivityDto(activity: List<Activity>): List<ActivityDto>
    fun mapActivityDto(activityDto: Activity): ActivityDto

    fun mapActivityInfoDto(activity: Activity?): ActivityInfoDto?
    @InheritInverseConfiguration

    fun mapActivityDto(activityDto: ActivityDto): Activity

    fun mapUserDto(user: User?): UserDto?
    fun mapUserDto(user: List<User>): List<UserDto>

    fun mapUserInfoDto(user: User?):UserInfoDto?
    fun mapUserImageDto(user: User): UserImageDto

    @InheritInverseConfiguration
    fun mapUserDto(user: UserDto): User

    @InheritInverseConfiguration
    fun mapUserInfoDto(userInfoDto: UserInfoDto): UserData

    @Mappings(
            Mapping(source = "user.jwtUser.username", target = "username"),
            Mapping(source = "user.jwtUser.authorities", target = "authorities")
    )
    fun mapUser(user: User): UserDto2

    fun mapAuthority(authority: Authority): AuthorityDto
    fun mapAuthority(authority: List<Authority>): List<AuthorityDto>


}