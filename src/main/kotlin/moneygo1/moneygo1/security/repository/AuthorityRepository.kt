package moneygo1.moneygo1.security.repository

import moneygo1.moneygo1.security.entity.Authority
import moneygo1.moneygo1.security.entity.AuthorityName
import org.springframework.data.repository.CrudRepository

interface AuthorityRepository: CrudRepository<Authority, Long> {
    fun findByName(input: AuthorityName): Authority
}