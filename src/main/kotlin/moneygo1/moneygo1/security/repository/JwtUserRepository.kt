package moneygo1.moneygo1.security.repository

import moneygo1.moneygo1.security.entity.JwtUser
import org.springframework.data.repository.CrudRepository

interface JwtUserRepository: CrudRepository<JwtUser, Long> {
    fun findByUsername(username: String): JwtUser
}