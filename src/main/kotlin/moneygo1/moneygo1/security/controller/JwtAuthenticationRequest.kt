package moneygo1.moneygo1.security.controller

data class JwtAuthenticationRequest(var username: String? = null,
                                    var password: String? = null)