package moneygo1.moneygo1.security.controller

data class JwtAuthenticationResponse(
        var token: String? = null
)