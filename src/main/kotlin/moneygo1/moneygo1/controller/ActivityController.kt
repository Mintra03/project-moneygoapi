package moneygo1.moneygo1.controller

import moneygo1.moneygo1.entity.Dto.ActivityDto
import moneygo1.moneygo1.entity.Dto.ActivityInfoDto
import moneygo1.moneygo1.entity.WalletStatus
import moneygo1.moneygo1.service.ActivityService
import moneygo1.moneygo1.util.MapperUtil
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import java.rmi.activation.ActivationGroupID

@RestController
class ActivityController{

    @Autowired
    lateinit var activityService: ActivityService

    @GetMapping("/activities")
    fun getAllActivities():ResponseEntity<Any> {
        val activities = activityService.getActivities()
        return ResponseEntity.ok(MapperUtil.INSTANCE.mapActivityDto(activities));
    }

    @GetMapping("/activities/query")
    fun getActivities(@RequestParam("name") name:String): ResponseEntity<Any>{
        var output = MapperUtil.INSTANCE.mapActivityDto(activityService.getActivitiesByPartialNameIgnoreCase(name))
        output?.let { return ResponseEntity.ok(it) }
        return ResponseEntity.ok(output)
    }

    @GetMapping("/activities/walletStatus")
    fun getActivitiesByWalletStatus(@RequestParam("walletStatus") walletStatus: WalletStatus): ResponseEntity<Any> {
        var output = MapperUtil.INSTANCE.mapActivityDto(activityService.getActivitiesByWalletStatus(walletStatus))
        return ResponseEntity.ok(output)
    }

    @GetMapping("/activities/category")
    fun getActivitiesByCategoryIgnoreCase(@RequestParam("category") category: String): ResponseEntity<Any> {
        var output = MapperUtil.INSTANCE.mapActivityDto(activityService.getActivitiesByCategoryIgnoreCase(category))
        return ResponseEntity.ok(output)
    }

    @GetMapping("/activities/money")
    fun getActivitiesByMoney(@RequestParam("money") money: Double): ResponseEntity<Any> {
        var output = MapperUtil.INSTANCE.mapActivityDto(activityService.getActivitiesByMoney(money))
        return ResponseEntity.ok(output)
    }

    @GetMapping("/activitiesUsername")
    fun getActivitiesByUserNameIgnoreCase(@RequestParam("name") name: String): ResponseEntity<Any>{
        val output = MapperUtil.INSTANCE.mapActivityDto(
                    activityService.getActivitiesByUserNameIgnoreCase(name))
        return ResponseEntity.ok(output)
    }

    @PostMapping("/activities/addAc/{activityId}")
    fun addActivities(@RequestBody activityDto: ActivityDto,
                      @PathVariable activityId: Long)
        : ResponseEntity<Any> {
        val output = activityService.save(MapperUtil.INSTANCE.mapActivityDto(activityDto))
        val outputDto = MapperUtil.INSTANCE.mapActivityDto(output)
        outputDto?.let { return ResponseEntity.ok(it) }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build()
    }

    @PutMapping("/activities/{activitiesId}")
    fun updateActivities(@PathVariable ("activitiesId") id: Long,
                         @RequestBody activityInfoDto: ActivityInfoDto)
            : ResponseEntity<Any> {
        val output = MapperUtil.INSTANCE.mapActivityInfoDto(activityService.saveInfo(id, activityInfoDto))
        output?.let { return ResponseEntity.ok(it) }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body("the activity id is not found")
    }

    @DeleteMapping("/activities/{activityId}")
    fun deleteActivities(@PathVariable("activityId") id: Long): ResponseEntity<Any> {
        val activity =activityService.remove(id)
        val outputActivity = MapperUtil.INSTANCE.mapActivityDto(activity)
        outputActivity?.let { return ResponseEntity.ok(it) }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body("the activity id is not found")
    }

}

