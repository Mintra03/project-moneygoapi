package moneygo1.moneygo1.controller

import moneygo1.moneygo1.entity.Dto.UserDto
import moneygo1.moneygo1.entity.Dto.UserImageDto
import moneygo1.moneygo1.entity.Dto.UserInfoDto
import moneygo1.moneygo1.service.AmazonClient
import moneygo1.moneygo1.service.UserService
import moneygo1.moneygo1.util.MapperUtil
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.multipart.MultipartFile

@RestController
class UserController {

    @Autowired
    lateinit var userService: UserService

    @GetMapping("/user")
    fun getAllUser(): ResponseEntity<Any> {
        val users = userService.getUsers()
        return ResponseEntity.ok(MapperUtil.INSTANCE.mapUserDto(users))
    }

    @GetMapping("/user/query")
    fun getUsersIgnoreCase(@RequestParam("name") name: String): ResponseEntity<Any> {
        var output = MapperUtil.INSTANCE.mapUserDto(userService.getUserByPartialNameIgnoreCase(name))
        output?.let { return ResponseEntity.ok(it) }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build()
    }

//    @GetMapping("/user/query/id")
//    fun getUsersId(@RequestParam("id") id: Long): ResponseEntity<Any> {
//        var output = MapperUtil.INSTANCE.mapUserDto(userService.getUserById(id))
//        output?.let { return ResponseEntity.ok(it) }
//        return ResponseEntity.status(HttpStatus.NOT_FOUND).build()
//    }

    @GetMapping("/user/name&email")
    fun getUserByNameIgnoreCaseAndEmailIgnoreCase(@RequestParam("name") name: String,
                              @RequestParam(value = "email", required = false) email: String?): ResponseEntity<Any> {
        var output = MapperUtil.INSTANCE.mapUserDto(userService.getUserByPartialNameIgnoreCaseAndEmailIgnoreCase(name, name))
        return ResponseEntity.ok(output)
    }

    @GetMapping("/username")
    fun getByNameEndingWith(@RequestParam("name") name: String): ResponseEntity<Any> {
        var output = MapperUtil.INSTANCE.mapUserDto(userService.getByNameEndingWith(name))
        return ResponseEntity.ok(output)
    }

    @GetMapping("/user/name")
    fun getUserWithPage(@RequestParam("name") name: String,
                        @RequestParam("page") page: Int,
                        @RequestParam("pageSize") pageSize: Int): ResponseEntity<Any> {
        val output = userService.getUserWithPage(name, page, pageSize)
        return ResponseEntity.ok(output
//                PageUserDto(
//                totalPages = output.totalPages,
//                totalElements = output.totalElements,
//                users = MapperUtil.INSTANCE.mapUserDto(output.content)
//        )
        )
    }

    @PostMapping("/user")
    fun addUser(@RequestBody userDto: UserDto)
            : ResponseEntity<Any> {
        val output = userService.save(MapperUtil.INSTANCE.mapUserDto(userDto))
        val outputDto = MapperUtil.INSTANCE.mapUserDto(output)
        outputDto?.let { return ResponseEntity.ok(it) }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build()
    }

    @PutMapping("/user/edit/data/{userId}")
    fun editUser(@PathVariable("userId") id: Long,
                 @RequestBody userInfoDto: UserInfoDto): ResponseEntity<Any> {
        val output = MapperUtil.INSTANCE.mapUserInfoDto(userService.saveInfo(id,userInfoDto))
        output?.let { return ResponseEntity.ok(it) }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body("the activity id is not found")
    }

    @Autowired
    lateinit var amazonClient: AmazonClient
    @PostMapping("/user/image/{userId}")
    fun uploadFile(@RequestPart("file") file: MultipartFile,
                   @PathVariable ("userId")id:Long ): ResponseEntity<*> {
        var imageUrl = this.amazonClient.uploadFile(file)
        var userId = userService.saveImage(id,imageUrl)
        val output= MapperUtil.INSTANCE.mapUserImageDto(userId)
        return ResponseEntity.ok(output)
    }

    @DeleteMapping("/users/{userId}")
    fun deleteUsers(@PathVariable("userId") id: Long): ResponseEntity<Any> {
        val user = userService.remove(id)
        val outputUser = MapperUtil.INSTANCE.mapUserDto(user)
        outputUser?.let { return ResponseEntity.ok(it) }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body("the activity id is not found")
    }
}
