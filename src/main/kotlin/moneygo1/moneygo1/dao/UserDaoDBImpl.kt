package moneygo1.moneygo1.dao

import moneygo1.moneygo1.entity.Dto.UserDto
import moneygo1.moneygo1.entity.User
import moneygo1.moneygo1.repository.UserRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.data.domain.PageRequest
import org.springframework.stereotype.Repository

@Profile("db")
@Repository
class UserDaoDBImpl: UserDao{
//    override fun getUserById(id: Long): List<User> {
//        return userRepository.
//    }

    override fun saveRegister(userRegister: User): User {
        return userRepository.save(userRegister)
    }

    override fun findById(id: Long): User? {
        return userRepository.findById(id).orElse(null)
    }

    override fun save(user: User): User {
        return userRepository.save(user)
    }

    override fun getUserWithPage(name: String, page: Int, pageSize: Int): List<User> {
        return userRepository.findByNameContainingIgnoreCase(name, PageRequest.of(page, pageSize))
    }

    override fun getByNameEndingWith(name: String): List<User> {
        return userRepository.findByNameEndingWith(name)
    }

    override fun getUserByPartialNameIgnoreCaseAndEmailIgnoreCase(name: String, email: String): List<User> {
        return userRepository.findByNameContainingIgnoreCaseOrEmailContainingIgnoreCase(name, email)
    }

    override fun getUserByPartialNameIgnoreCase(name: String): User? {
        return userRepository.findByNameIgnoreCase(name)
    }

    @Autowired
    lateinit var userRepository: UserRepository
    override fun getUsers(): List<User> {
        return userRepository.findAll().filterIsInstance(User::class.java)
    }
}