package moneygo1.moneygo1.dao

import moneygo1.moneygo1.entity.Activity
import moneygo1.moneygo1.entity.WalletStatus
import org.springframework.context.annotation.Description
import org.springframework.context.annotation.Profile

//@Profile("mem")
interface ActivityDao {
    fun getActivities(): List<Activity>
//    fun getActivitiesEdit(money: Double, balance: Double): Activity
    fun getActivitiesByPartialNameIgnoreCase(name: String): List<Activity>
    fun getActivitiesByWalletStatus(walletStatus: WalletStatus): List<Activity>
    fun getActivitiesByCategoryIgnoreCase(category: String): List<Activity>
    fun getActivitiesByMoney(money: Double): List<Activity>
    fun getActivitiesByUserNameIgnoreCase(name: String) :List<Activity>
    fun save(activity: Activity): Activity
    fun findById(id: Long): Activity

}