package moneygo1.moneygo1.dao

import moneygo1.moneygo1.entity.Activity
import moneygo1.moneygo1.entity.WalletStatus
import moneygo1.moneygo1.repository.ActivityRepository
import moneygo1.moneygo1.repository.UserRepository
import moneygo1.moneygo1.service.ActivityService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Repository

@Profile("db")
@Repository
class ActivityDaoDBImpl:ActivityDao{
//    override fun getActivitiesEdit(money: Double, balance: Double): Activity {
//        return activityRepository.save()
//    }

    override fun findById(id: Long): Activity {
        return activityRepository.findById(id).orElse(null)
    }

    override fun save(activity: Activity): Activity {
        return activityRepository.save(activity)
    }

    override fun getActivitiesByUserNameIgnoreCase(name: String): List<Activity> {
        return activityRepository.findByUser_NameContainingIgnoreCase(name)
    }

    override fun getActivitiesByMoney(money: Double): List<Activity> {
        return activityRepository.findByMoney(money)
    }

    override fun getActivitiesByCategoryIgnoreCase(category: String): List<Activity> {
        return activityRepository.findByCategoryIgnoreCase(category)
    }

    override fun getActivitiesByWalletStatus(walletStatus: WalletStatus): List<Activity> {
        return activityRepository.findByWalletStatus(walletStatus)
    }

    override fun getActivitiesByPartialNameIgnoreCase(name: String): List<Activity> {
        return activityRepository.findByNameContainingIgnoreCase(name)
    }

    @Autowired
    lateinit var activityRepository: ActivityRepository
    override fun getActivities(): List<Activity> {
//        return activityRepository.findAll().filterIsInstance(Activity::class.java)
        return activityRepository.findByIsDeletedIsFalse()
    }
}