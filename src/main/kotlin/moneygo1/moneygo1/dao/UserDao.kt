package moneygo1.moneygo1.dao

import moneygo1.moneygo1.entity.Dto.UserDto
import moneygo1.moneygo1.entity.User

interface UserDao {
    fun getUsers(): List<User>
    fun getUserByPartialNameIgnoreCase(name: String): User?
    fun getUserByPartialNameIgnoreCaseAndEmailIgnoreCase(name: String, email: String): List<User>
    fun getByNameEndingWith(name: String): List<User>
    fun getUserWithPage(name: String, page: Int, pageSize: Int): List<User>
    fun save(user: User): User
    fun findById(id: Long): User?
    fun saveRegister(userRegister: User): User
//    fun getUserById(id: Long): List<User>
}